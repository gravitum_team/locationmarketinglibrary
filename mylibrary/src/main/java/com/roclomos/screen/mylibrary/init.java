package com.roclomos.screen.mylibrary;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.RemoteException;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by True Colors on 02-11-2015.
 */
public class init implements BeaconConsumer {
private String advertisingId;
    private Context context;
    protected static final String TAG = "MonitoringActivity";
    private BeaconManager beaconManager;
public init(Context context)
{
File beaconlist;
    this.context=context;
    verifyBluetooth();
    beaconManager = BeaconManager.getInstanceForApplication(context);
    //beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:0-3=2d24bf16"));
    beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"));
    beaconManager.bind(this);
    new Thread(new Runnable() {
        public void run() {
            try {
                AdvertisingIdClient.AdInfo adInfo = AdvertisingIdClient.getAdvertisingIdInfo(init.this.context);
                advertisingId = adInfo.getId();
              //  optOutEnabled = adInfo.isLimitAdTrackingEnabled();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }).start();

}
    private void verifyBluetooth() {

        try {
            if (!BeaconManager.getInstanceForApplication(context).checkAvailability()) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Bluetooth not enabled");
                builder.setMessage("Please enable bluetooth in settings and restart this application.");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        System.exit(0);
                    }
                });
                builder.show();
            }
        }
        catch (RuntimeException e) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Bluetooth LE not available");
            builder.setMessage("Sorry, this device does not support Bluetooth LE.");
            builder.setPositiveButton(android.R.string.ok, null);
            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

                @Override
                public void onDismiss(DialogInterface dialog) {
                  //  finish();
                    System.exit(0);
                }

            });
            builder.show();

        }

    }
    private Beacon infield=null,left=null;
    long starttime=0,endtime,timespent;
    Boolean moveout=false,first=true;
    String UUID,major,minor;
    double dist;
    @Override
    public void onBeaconServiceConnect() {


        beaconManager.setRangeNotifier(new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
                for(final Beacon b : beacons)
                {
                     UUID = b.getId1().toString();major=b.getId2().toString();minor=b.getId3().toString();
dist = b.getDistance();
                    if (dist<2 && infield!=b)
                    {
                        left=infield;
infield=b;
                        if(starttime!=0)
                        timespent=System.currentTimeMillis()-starttime;
                        starttime=System.currentTimeMillis();
                        if(first)
                        first=false;
                        else
                        {
                            RequestQueue queue = Volley.newRequestQueue(context);
                            StringRequest sr = new StringRequest(Request.Method.POST,"http://demo.roclomos.com/lib/dwell.php", new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    //mPostCommentResponse.requestCompleted();
                                 //   lvb.setEnabled(true);
                                   // Toast.makeText(MainActivity.this, response, Toast.LENGTH_SHORT).show();
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    //mPostCommentResponse.requestEndedWithError(error);
                                   // Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }){
                                @Override
                                protected Map<String,String> getParams(){
                                    Map<String,String> params = new HashMap<String, String>();

                                  //  Spinner spn =(Spinner)findViewById(R.id.spinner);
                                   // String names= spn.getSelectedItem().toString();
                                   // String ids = id.get(name.indexOf(names));
                                    // params.put("name",names);
                                    params.put("id",advertisingId);
                                    params.put("uuid",left.getId1().toString());
                                    params.put("major",left.getId2().toString());
                                    params.put("minor",left.getId3().toString());

                                    params.put("dwelltime",timespent+"");
                                   // params.put("mac",bmac.getText().toString().replace("Mac Add:", ""));
                                   // params.put("bdist",bdist.getText().toString().replace("Distance:", ""));
                                   // params.put("rssi",brssi.getText().toString().replace("RSSI:", ""));
                                    //params.put("about",about.getText().toString());
                                    return params;
                                }

                                @Override
                                public Map<String, String> getHeaders() throws AuthFailureError {
                                    Map<String,String> params = new HashMap<String, String>();
                                    params.put("Content-Type","application/x-www-form-urlencoded");
                                    return params;
                                }
                            };
                            queue.add(sr);


                        }
                    }


                    RequestQueue queue = Volley.newRequestQueue(context);
                    StringRequest sr = new StringRequest(Request.Method.POST,"http://demo.roclomos.com/lib/insert.php", new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            //mPostCommentResponse.requestCompleted();
                            //   lvb.setEnabled(true);
                            // Toast.makeText(MainActivity.this, response, Toast.LENGTH_SHORT).show();
                            if(response.isEmpty())
                                return;
String img = response.split("@@@")[0],msg=response.split("@@@")[1];
RCLMSNotificationAdvt rclmsNotificationAdvt = new RCLMSNotificationAdvt();
                            rclmsNotificationAdvt.detected(img,msg);



                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //mPostCommentResponse.requestEndedWithError(error);
                            // Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }){
                        @Override
                        protected Map<String,String> getParams(){
                            Map<String,String> params = new HashMap<String, String>();

                            //  Spinner spn =(Spinner)findViewById(R.id.spinner);
                            // String names= spn.getSelectedItem().toString();
                            // String ids = id.get(name.indexOf(names));
                            // params.put("name",names);
                            //params.put("id",ids);
                            params.put("uuid",b.getId1().toString());
                            params.put("major",b.getId2().toString());
                            params.put("minor",b.getId3().toString());
                            params.put("id",advertisingId);
                          //  params.put("dwelltime",timespent+"");
                            // params.put("mac",bmac.getText().toString().replace("Mac Add:", ""));
                            // params.put("bdist",bdist.getText().toString().replace("Distance:", ""));
                            // params.put("rssi",brssi.getText().toString().replace("RSSI:", ""));
                            //params.put("about",about.getText().toString());
                            return params;
                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String,String> params = new HashMap<String, String>();
                            params.put("Content-Type","application/x-www-form-urlencoded");
                            return params;
                        }
                    };
                    queue.add(sr);









                    Log.i(TAG, b.getId3().toString());
                }

            }
        });

        try {
            beaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", null, null, null));
        } catch (RemoteException e) {    }

    }

    @Override
    public Context getApplicationContext() {
        return null;
    }

    @Override
    public void unbindService(ServiceConnection serviceConnection) {

    }

    @Override
    public boolean bindService(Intent intent, ServiceConnection serviceConnection, int i) {
        return false;
    }

    public void close()
    {
        beaconManager.unbind(this);
    }

}
