package com.roclomos.screen.mylibrary;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by True Colors on 04-11-2015.
 */
public class RCLMSNotificationAdvt {
Context context;
    private MyCustomObjectListener listener;
    public RCLMSNotificationAdvt() {
        this.listener = null;







    }
    public void setCustomObjectListener(MyCustomObjectListener listener) {
        this.listener = listener;
    }

    public void detected(String img,String msg)
    {
        listener.onDataLoaded(img,msg);
    }


    public interface MyCustomObjectListener {
        // These methods are the different events and
        // need to pass relevant arguments related to the event triggered
      //  public void onObjectReady(String title);
        // or when data has been loaded
        public void onDataLoaded(String img,String msg);
    }
}
